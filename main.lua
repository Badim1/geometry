_W = display.contentWidth;
_H = display.contentHeight;

optionsBuild = "ios";-- "ios"/"android"/"amazon"/"nook"


scaleDevice = 1;
scaleGraphics = 1;

CMathExt = require("eliteMath").new();

	local ScreenGame = display.newGroup();
	local tempScreen = display.newGroup();

	local rDD = 60*60;
	local power_min = 0;
	local power_max = 20*scaleGraphics;
	local bAtk = false;
	local idAtk = nil;
	local idMove = nil;
	local points = {};
	
	local function drawDashedLine(tar,p1,p2)
		local mc = display.newGroup();
		tar:insert(mc);
		local dx=p2.x-p1.x;
		local dy=p2.y-p1.y;
		local d = math.sqrt(dx*dx+dy*dy);
		local ts = 10;
		local l = math.floor(d/ts);
		
		for i=0,l,2 do
			local line = display.newLine(mc, p1.x + dx*i/l, p1.y + dy*i/l, p1.x + dx*(i+1)/l, p1.y + dy*(i+1)/l);
			line.strokeWidth = 2;
		end
		
		return mc
	end
	
	local function createPoint(tx, ty)
		local mc = display.newGroup();
		mc.x,mc.y = tx,ty;
		local point = display.newCircle(mc, 0, 0, 10 )
		point:setFillColor(255,255,255);
		table.insert(points, mc);
		return mc
	end
	
	local p1=createPoint(50+math.random()*(_W-100), 50+math.random()*600);
	local p2=createPoint(50+math.random()*(_W-100), 50+math.random()*600);
	local p3=createPoint(50+math.random()*(_W-100), 50+math.random()*600);
	local p4=createPoint(50+math.random()*(_W-100), 50+math.random()*600);
	
	local p10 = createPoint(50+math.random()*(_W-100), 50+math.random()*600);
	local p11 = createPoint(50+math.random()*(_W-100), 50+math.random()*600);
	
	local c1=display.newGroup();
	c1.x,c1.y = _W*1/4,_H*2/4;
	c1.alpha = 0.5;
	c1.r = 90;
	local cir=display.newRect(c1, 0, 0, c1.r*2, c1.r*2);
	cir.x,cir.y = 0,0;
	display.newCircle(c1, 0, 0, 10 )
	table.insert(points, c1);
	
	local c2=display.newGroup();
	c2.x,c2.y = _W*3/4,_H*2/4;
	c2.alpha = 0.5;
	c2.r = 90;
	local cir=display.newCircle(c2, 0, 0, c2.r);
	cir.x,cir.y = 0,0;
	display.newCircle(c2, 0, 0, 10 )
	table.insert(points, c2);
	
	
	
	local balistic_i,balistic_m=0,60;
	
	local line1;
	local line2;
	local line3;
	local function draw_temp_p(obj)
		p5 = display.newCircle(tempScreen, 0, 0, 8 );
		p5.x,p5.y=obj.x,obj.y;
		p5:setFillColor(255,0,0);
	end
	local function update()
		for i=tempScreen.numChildren, 1, -1 do 
			tempScreen[i]:removeSelf();
		end
		
		line1=display.newLine(tempScreen, p1.x, p1.y, p2.x, p2.y);
		line1.strokeWidth=2;
		line2=display.newLine(tempScreen, p3.x, p3.y, p4.x, p4.y);
		line2.strokeWidth=2;
		
		line3=display.newLine(tempScreen, p10.x, p10.y, p11.x, p11.y);
		line3:setStrokeColor(1,0,1,0.3);
		line3.strokeWidth=2;
		
		drawDashedLine(tempScreen, p10, p11);

		
		local dx, dy = p11.x - p10.x, p11.y - p10.y;
		local velocity = 0.5;
		local d = math.sqrt(dx*dx+dy*dy);
		local h = 150;
		local gravity = 0.3;
		local g = gravity;
		
		-- elevation
		local h0 = dy;
		local t = math.sqrt(2*g*(h-h0))/g + math.sqrt(2*g*h)/g;
		local velocity = math.sqrt(math.pow(d/t,2) + 2*g*(h-h0));
		local a = math.atan(t*math.sqrt(2*g*(h-h0))/d);
		
		--same level:
		-- local a = math.atan(4*h/d);
		-- local t = 2*math.sqrt(2*(h)/g);
		-- local velocity = math.sqrt(2*g*h)/math.sin(a);
		

		local sx,sy = p10.x, p10.y;
		local vx,vy = velocity*math.cos(a), -velocity*math.sin(a);
		for i=0,t do
			vy = vy + gravity;
			local nx, ny = sx+vx, sy+vy;
			local line=display.newLine(tempScreen, sx, sy, nx, ny);
			sx, sy = nx, ny;
		end
		
		-- float GetAngle(float height, Vector3 startLocation, Vector3 endLocation)
    -- {
        -- float range = math.sqrt(math.Pow(startLocation.x - endLocation.x,2) + math.Pow(startLocation.z - endLocation.z,2));
        -- float offsetHeight = endLocation.y - startLocation.y;
        -- float g = -Physics.gravity.y;

        -- float verticalSpeed = math.sqrt(2 * gravity * height);
        -- float travelTime = math.sqrt(2 * (height - offsetHeight) / g) + math.sqrt(2 * height / g);
        -- float horizontalSpeed = range / TravelTime;
        -- float velocity = math.sqrt(math.Pow(verticalSpeed,2) +  math.Pow(horizontalSpeed, 2));

        -- return -math.Atan2(verticalSpeed / velocity, horizontalSpeed / velocity) + math.PI;
    -- }
		
		-- draw_temp_p({x=p10.x + balistic_i*dx/balistic_m, y=p10.y + balistic_i*dy/balistic_m - 100*(1-math.abs(balistic_i-balistic_m/2)/balistic_m/2)});
		-- balistic_i = balistic_i+1;
		-- if(balistic_i>balistic_m)then
			-- balistic_i = 0; 
		-- end
		
		
		local obj = CMathExt:InterLineLine(p1, p2, p3, p4);
		if(obj)then
			draw_temp_p(obj);
		end
		
		
		local arr = CMathExt:InterLineSqr(p1, p2, c1, c1.r);
		if(#arr>0)then
			for i=1,#arr do
				draw_temp_p(arr[i]);
			end
		end
		
		
		local arr = CMathExt:InterRayCir(p1, p2, c2, c2.r);
		if(#arr>0)then
			for i=1,#arr do
				draw_temp_p(arr[i]);
			end
		end
		
		
		local arr = CMathExt:InterLineSqr(p3, p4, c1, c1.r);
		if(#arr>0)then
			for i=1,#arr do
				draw_temp_p(arr[i]);
			end
		end
		
		
		local arr = CMathExt:InterRayCir(p3, p4, c2, c2.r);
		if(#arr>0)then
			for i=1,#arr do
				draw_temp_p(arr[i]);
			end
		end
	end

	local touchAct;
	local touchTar;
	local function touchHandler(e)
		local phase = e.phase;
		if(phase=='began')then
			for i=1,#points do
				local p=points[i];
				local dx=p.x-e.x;
				local dy=p.y-e.y;
				local dd=dx*dx+dy*dy;
				if(dd<rDD)then
					touchAct='moving';
					touchTar=p;
				end
			end
		elseif(phase=='moved')then
			if(touchAct=='moving')then
				touchTar.x,touchTar.y = e.x,e.y;
			end
		else
			touchAct = nil;
		end
		update();
	end
	
	update();
	
	Runtime:addEventListener( "touch", touchHandler );
	ScreenGame:insert(tempScreen);